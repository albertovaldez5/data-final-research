- [Kardias](#kardias)
  - [Kardias Report 2021](#kardias-report-2021)
- [Tech Requirements](#tech-requirements)
  - [Data Problems to Solve](#data-problems-to-solve)
  - [Deploy ML Models](#deploy-ml-models)
  - [Alternative Hosting Services](#alternative-hosting-services)
  - [AWS Healthcare & Life](#aws-healthcare-life)
  - [Data Bootcamp Agenda](#data-bootcamp-agenda)
  - [Data Bootcamp Tech Stack](#data-bootcamp-tech-stack)
- [Learning about ML in Healthcare](#learning-about-ml-in-healthcare)
  - [Clinical Decision Support Systems](#clinical-decision-support-systems)
  - [Explainability for A.I. in Healthcare](#explainability-for-a-i-in-healthcare)
  - [Congenital Heart Disease](#congenital-heart-disease)
  - [MIMIC-III Database](#mimic-iii-database)
  - [Privacy-Preserving Machine Learning (New Field)](#privacy-preserving-machine-learning-new-field)
- [Biometrics and Analytics](#biometrics-and-analytics)
  - [Bland-Altman plot](#bland-altman-plot)
- [MIT Class: Applications of ML in Cardiac Imaging](#mit-class-applications-of-ml-in-cardiac-imaging)
  - [Types of Heart Diseases](#types-of-heart-diseases)
  - [Cardiac imaging technologies](#cardiac-imaging-technologies)
  - [Cardiac decisions guided by inputs from imaging](#cardiac-decisions-guided-by-inputs-from-imaging)
  - [How Medical Imaging Data Are Stored](#how-medical-imaging-data-are-stored)
  - [Access to Data](#access-to-data)
  - [Characteristics of Cardiac Imaging Data](#characteristics-of-cardiac-imaging-data)
  - [Interpreting data](#interpreting-data)
  - [Solutions to look for](#solutions-to-look-for)
  - [Low risk - high reward automated interpretation](#low-risk-high-reward-automated-interpretation)
  - [What should we focus on?](#what-should-we-focus-on)
  - [How do we show improvement?](#how-do-we-show-improvement)
  - [Automated Disease Detection Models](#automated-disease-detection-models)
  - [Sensitivity](#sensitivity)
  - [Accuracy](#accuracy)
  - [Clinical Deployment](#clinical-deployment)
  - [Future Improvements](#future-improvements)

If images don&rsquo;t show up, go [here.](https://gitlab.com/albertovaldez5/data-final-research/-/blob/main/docs/research.md)


<a id="kardias"></a>

# Kardias

Kardias: <https://kardias.org/nosotros/>

> Somos una asociación civil dedicada a desarrollar programas de excelencia para la atención de niñas y niños con enfermedades cardíacas, así como a promover la educación y la difusión sobre la relevancia de las cardiopatías congénitas.

Kardias Blog: <https://kardias.org/blog/>

Friends of Kardias: <https://friendsofkardias.org/>


<a id="kardias-report-2021"></a>

## Kardias Report 2021

![img](../resources/kardias1.png)

<https://kardias.org/wp-content/uploads/2022/10/INFORME_2021_BAJA.pdf>

El reporte de Kardias 2021 incluye datos sobre las diferentes consultas y estudios realizados.

![img](../resources/kardias2.png)

![img](../resources/kardias3.png)


<a id="tech-requirements"></a>

# Tech Requirements


<a id="data-problems-to-solve"></a>

## Data Problems to Solve

Current Data Storage:

1.  CSV files?
2.  DICOM files?
3.  Database?
4.  In-premises server?
5.  IT / Web services?


<a id="deploy-ml-models"></a>

## Deploy ML Models

Amazon Sagemaker. <https://aws.amazon.com/sagemaker/>

Azure ML. <https://azure.microsoft.com/en-us/products/machine-learning/>


<a id="alternative-hosting-services"></a>

## Alternative Hosting Services

<https://www.heroku.com/>

Emergent alternatives for what was Heroku and cheaper than Azure, AWS, Google.

<https://harperdb.io/>

<https://www.digitalocean.com/>

<https://www.mongodb.com/atlas>

On-premises server? (highest security, long-term investment, will need IT consultant).


<a id="aws-healthcare-life"></a>

## AWS Healthcare & Life

Secure and reliable deployment for data pipeline.

<https://aws.amazon.com/health/machine-learning/?trk=65fe25a3-735f-43d5-8c08-6c5c926b009f&sc_channel=ps&s_kwcid=AL!4422!3!608595092146!p!!g!!machine%20learning%20and%20healthcare&ef_id=CjwKCAjwh4ObBhAzEiwAHzZYU_5fOG-nkcw7Bjoua9-VSvlZH_PwqlgKN306Et7LqCYQgxbIkCf_PhoCZkoQAvD_BwE:G:s&s_kwcid=AL!4422!3!608595092146!p!!g!!machine%20learning%20and%20healthcare>


<a id="data-bootcamp-agenda"></a>

## Data Bootcamp Agenda

Time and resources consideration.

-   **First Segment:** Sketch It Out: Decide on your overall project, select your question, and build a simple model. You&rsquo;ll connect the model to a fabricated database, using comma-separated values (CSV) or JavaScript Object Notation (JSON) files, to prototype your idea.
-   **Second Segment:** Build the Pieces: Train your model and build out the database you&rsquo;ll use for your final presentation.
-   **Third Segment:** Plug It In: Connect your final database to your model, continue to train your model, and create your dashboard and presentation.
-   **Fourth Segment:** Put It All Together: Put the final touches on your model, database, and dashboard. Lastly, create and deliver your final presentation to your class.

Rubric:

1.  Presentation
2.  GitHub repo
3.  ML Model
4.  Database Integration
5.  Dashboard


<a id="data-bootcamp-tech-stack"></a>

## Data Bootcamp Tech Stack

Example of a Tech Stack. Taken from the Data Analytics Bootcamp Materials.

```md
# Technologies Used
## Data Cleaning and Analysis
Pandas will be used to clean the data and perform an exploratory analysis. Further analysis will be completed using Python.

## Database Storage
Mongo is the database we intend to use, and we will integrate Flask to display the data.

## Machine Learning
SciKitLearn is the ML library we'll be using to create a classifier. Our training and testing setup is ___. Extra ML verbiage here.

## Dashboard
In addition to using a Flask template, we will also integrate D3.js for a fully functioning and interactive dashboard. It will be hosted on ___.
```


<a id="learning-about-ml-in-healthcare"></a>

# Learning about ML in Healthcare


<a id="clinical-decision-support-systems"></a>

## Clinical Decision Support Systems

Clinical decision support systems (CDSS) are computer-based programs that analyze data within EHRs to provide prompts and reminders to assist health care providers in implementing evidence-based clinical guidelines at the point of care. <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>

Coursera 8 hours course (part of larger specialization).

<https://www.coursera.org/learn/cdss4>


<a id="explainability-for-a-i-in-healthcare"></a>

## Explainability for A.I. in Healthcare

Model explainability refers to the concept of being able to understand the machine learning model. For example – If a healthcare model is predicting whether a patient is suffering from a particular disease or not. The medical practitioners need to know what parameters the model is taking into account or if the model contains any bias. So, it is necessary that once the model is deployed in the real world. Then, the model developers can explain the model. <sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>

Article on A.I. Explainability in Healthcare:

<https://bmcmedinformdecismak.biomedcentral.com/articles/10.1186/s12911-020-01332-6>

Explainability for ECG data.

<https://modulai.io/blog/explainability-for-ecg-data/>

ECG Classification.

<https://www.kaggle.com/code/polomarco/ecg-classification-cnn-lstm-attention-mechanism>


<a id="congenital-heart-disease"></a>

## Congenital Heart Disease

Congenital heart disease is a general term for a range of birth defects that affect the normal way the heart works.

-   <https://www.nhs.uk/conditions/congenital-heart-disease/>

-   <https://en.wikipedia.org/wiki/Congenital_heart_defect>


<a id="mimic-iii-database"></a>

## MIMIC-III Database

MIMIC-III is a large, freely-available database comprising deidentified health-related data associated with over forty thousand patients who stayed in critical care units of the Beth Israel Deaconess Medical Center between 2001 and 2012. The database includes information such as demographics, vital sign measurements made at the bedside (~1 data point per hour), laboratory test results, procedures, medications, caregiver notes, imaging reports, and mortality (including post-hospital discharge).

<https://physionet.org/content/mimiciii/1.4/>


<a id="privacy-preserving-machine-learning-new-field"></a>

## Privacy-Preserving Machine Learning (New Field)

**PPML (Research):**

The PPML initiative was started in partnership between Microsoft Research and Microsoft product teams with the objective of protecting the confidentiality and privacy of customer data when training large-capacity language models.

<https://www.microsoft.com/en-us/research/blog/privacy-preserving-machine-learning-maintaining-confidentiality-and-preserving-trust/> **Predictive Analysis in Encrypted Data (Paper):**

Privacy of sensitive information can be guaranteed, if it is encrypted by the data owner before being uploaded to a cloud service. In that way, only the legitimate data owner can access the data by decrypting it using their private decryption key.

<https://www.microsoft.com/en-us/research/wp-content/uploads/2014/08/336-1.pdf>


<a id="biometrics-and-analytics"></a>

# Biometrics and Analytics


<a id="bland-altman-plot"></a>

## Bland-Altman plot

Bland-Altman plot is a method of data plotting used in analyzing the agreement between two `assays` <sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup> <sup>, </sup><sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup>.

Bland–Altman plots are extensively used to evaluate the agreement among two different instruments or two measurements techniques. It can be used to compare a new measurement technique or method with a `gold standard` <sup><a id="fnr.5" class="footref" href="#fn.5" role="doc-backlink">5</a></sup>. Bland-Altman plots are available in R libraries <sup><a id="fnr.6" class="footref" href="#fn.6" role="doc-backlink">6</a></sup>.

![img](https://d2mvzyuse3lwjc.cloudfront.net/doc/en/UserGuide/images/Bland_Altman_Plots/UG_appendix_bland_altman_plot.png)


<a id="mit-class-applications-of-ml-in-cardiac-imaging"></a>

# MIT Class: Applications of ML in Cardiac Imaging

<https://www.youtube.com/watch?v=MoEaRpLNo9A&ab_channel=MITOpenCourseWare>

MIT 6.S897 Machine Learning for Healthcare, Spring 2019 Instructor: Rahul Deo.

Guest lecturer Rahul Deo, the lead investigator of the One Brave Idea project at Brigham and Women&rsquo;s Hospital, talks about how machine learning techniques are being used and can be used further to augment cardiac imaging.

These notes follow the video:


<a id="types-of-heart-diseases"></a>

## Types of Heart Diseases

10:20 <sup><a id="fnr.7" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

| Abnormality           | Disease                                                                    | Presentation                                    | Treatment                                            |
|--------------------- |-------------------------------------------------------------------------- |----------------------------------------------- |---------------------------------------------------- |
| Contractile function  | Heart failure                                                              | Shortness of breath, fluid buildup in legs      | Medications, ventricular assist device, transplant   |
| Coronary blood supply | Coronary artery disease, myocardial infraction                             | Chest pain, shortness of breath                 | Angioplasty/stening, coronary artery bypass grafting |
| Circulatory Flow      | Aortic stenosis/regurgitation, mitral stenosis/regurgitation.              | Light headedness, shortness of breath, fainting | valve replacement, valve repair                      |
| Heart rhythm          | Arterial fibrillation/flutter, ventricular tachycardia, sick sinus synrome | palpitations, fainting, cardiac arrest          | ablation, implantable defibrillator, pacemaker       |


<a id="cardiac-imaging-technologies"></a>

## Cardiac imaging technologies

12:42 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

| Modality                         | Cost      | Approach                 | Diagnostic Utility                                                                                      |
|-------------------------------- |--------- |------------------------ |------------------------------------------------------------------------------------------------------- |
| Electrocardiogram (ECG)          | low       | Voltage Differences      | Mycocardial infarction                                                                                  |
| Echocardiography                 | mid-high  | Ultrasound (sound waves) | Quantitation of cardiac structure and function, heart failure, valvular disease, pulmonary hypertension |
| MRI                              | very high | Magnetic resonance       | Quantitation of cardiac structure and function, heart failure, valvular disease                         |
| Angiography                      | very high | X-ray                    | Epicardial coronary artery disease                                                                      |
| SPECT/PET (Non-invasive)         | very high | Radionuclide tracer      | Coronary artery disease, microvascular disease                                                          |
| Intracardiac pressure transducer | high      | Pressure transucer       | Heart failure, valvular disease, pulmonary hypertension                                                 |


<a id="cardiac-decisions-guided-by-inputs-from-imaging"></a>

## Cardiac decisions guided by inputs from imaging

14:25 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

| Disease                 | Decision                                                                | Inputs                                                               |
|----------------------- |----------------------------------------------------------------------- |-------------------------------------------------------------------- |
| heart failure           | decision to implant a defibrilator to prevent sudden death              | Symptoms + ejection fraction of the heart <35%                       |
| Coronary artery disease | Angioplasty and stenting of a coronary artery                           | Symptoms + stenosis > 70%                                            |
| Aortic stenosis         | Valve replacement                                                       | Symptoms + valve are + enlargement of the heart                      |
| Atrial fibrillation     | Decision to start anticoagulation to prevent stroke                     | Age, sex, other diagnoses                                            |
| Mycrocardial infraction | Decision to start aspirin and a statin to prevent a future heart attack | A risk model based on age, sex, lab values, blood pressure, diabetes |


<a id="how-medical-imaging-data-are-stored"></a>

## How Medical Imaging Data Are Stored

**DICOM** <sup><a id="fnr.8" class="footref" href="#fn.8" role="doc-backlink">8</a></sup> is the international standard to transmit, store, retrieve, print, process and display imaging information.

![img](https://www.radiantviewer.com/dicom-viewer-manual/images/radiant_dicom_viewer_dicom_tags_window.png)

Image/video files are stored in DICOM format and combine a compressed image with a DICOM header which includes characteristics of the image.

Open access libraries like GDCM, pydicom facilitate compressing/uncompressing, reading and editing headers. <sup><a id="fnr.9" class="footref" href="#fn.9" role="doc-backlink">9</a></sup> More info on using PyDicom. <sup><a id="fnr.10" class="footref" href="#fn.10" role="doc-backlink">10</a></sup>

Osirix is a high-end medical images viewer <sup><a id="fnr.11" class="footref" href="#fn.11" role="doc-backlink">11</a></sup>.


<a id="access-to-data"></a>

## Access to Data

Most imaging data is housed in data archives, not very accessible to do Machine Learning / Data processes. Access is often highly limited, some images contain patient information. Labels and measurements are often stored separately in the electronic health record.

**Challenge**: It is expensive to collect this type of data and the data available may be limited. The higher the costs of the study/imaging, the less data available.


<a id="characteristics-of-cardiac-imaging-data"></a>

## Characteristics of Cardiac Imaging Data

18:00 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

1.  Compression: lossy vs losseless.
2.  Spatial resolution: pixel dimension.
3.  Sampling Frequency: temporal resolution.
4.  Coronary artery velocity is 10-65 mm/seconds.
5.  Gating: Electrocardiogram info. can be coupled with imaging information to average images accross corresponding portions of the cycle (a sort of workaround with missing data, low frequency resolution).


<a id="interpreting-data"></a>

## Interpreting data

19:00 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

The current way to interpret the data is to measure it &ldquo;by hand&rdquo;. So diagnosis is made by looking at the images. Machine Learning can help with Image/Video classification, Semantic Segmentation, Image registration (all use Convolutional Neural Networks).

If a radiologist takes 2 minutes to read a study, how much benefit is the to automate the process?

Some answers: In some scenarios like overnight/weekend diagnosis/reading it can be helpful. An independent read can catch some missed diagnoses.

Example of DL Imagen Segmentation. <sup><a id="fnr.12" class="footref" href="#fn.12" role="doc-backlink">12</a></sup>

![img](https://www.frontiersin.org/files/Articles/508599/fcvm-07-00025-HTML/image_m/fcvm-07-00025-g001.jpg)


<a id="solutions-to-look-for"></a>

## Solutions to look for

36:05 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

1.  Low-cost quantitative metrics: track indicators of disease progression and reflect the onset of tissue-level changes.
2.  Specific to the disease process.
3.  Combine resultant data with therapy that matches the report, not only numbers (not in our hands as analytics).

Example quantitative measures:

1.  Simple cardiac ultrasound views provide a quantitative metric of early disease progression (changes on ventricular mass, volume, etc.)


<a id="low-risk-high-reward-automated-interpretation"></a>

## Low risk - high reward automated interpretation

38:00 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

What kind of medical procedures or applications can automated-metrics accompany? Low liability and that can help the patients in not-so-critical/high-skilled tasks.

| Low liability                                               | High liability                      |
|----------------------------------------------------------- |----------------------------------- |
| Non-skilled acquisition (primary care)                      | Skilled sonographer                 |
| Low cost handheld ultrasound                                | Costly full ultrasound system       |
| Automated Interpretation                                    | Expert Cardiologist interpretation  |
| Early in disease course                                     | Late in disease course              |
| Decision support (initiation or intensification of therapy) | Difficult decision regarding sugery |


<a id="what-should-we-focus-on"></a>

## What should we focus on?

39:00 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

-   Automating interpretation to reduce costs.
-   Low-cost quantitative metrics that are indicative of disease progression and reflect the onset of these tissue-level changes.
-   Recognizing meaningful subclasses of diseases. Capture complex underlying biological processes.

**Rapid triage**: Detecting markers of myocardial infarction pattern in electrocardiogram (ECG). Offload rapid triage decisions to an automated system that can be used by non-cardiologists.

**Echocardiography** classification <sup><a id="fnr.13" class="footref" href="#fn.13" role="doc-backlink">13</a></sup>.


<a id="how-do-we-show-improvement"></a>

## How do we show improvement?

50:33 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

There is a wide variability in measurement values from physician to physician, up to 8-9% for the ejection fraction <sup><a id="fnr.14" class="footref" href="#fn.14" role="doc-backlink">14</a></sup>, the ejection fraction normal value is around 60%.

1.  Compare to an average of multiple readers.
2.  Compare to a gold-standard imaging system (MRI).
3.  Demonstrate utility in outcomes (trials).


<a id="automated-disease-detection-models"></a>

## Automated Disease Detection Models

54:03 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

Several rare diseases would benefit from referral to a cardiologist or specialty center. These diagnoses tend to be missed at centers that see them infrequently. ML models can serve as &ldquo;decision support&rdquo;.

![img](../resources/mit2.png)

![img](../resources/mit3.png)

![img](../resources/mit4.png)


<a id="sensitivity"></a>

## Sensitivity

57:55 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

What should be the True-Positive False-Positive Trade-off?

> Air on the side of higher sensitivity.

Some of the models are for pretty rare diseases, so it depends how much you want cardiologists to go back and take a look at some studies/cases again. Depends on the costs of going back and looking at cases that may be false positives. But if the sensitivity is good, we can make the case for spending the time and money to revisit them.


<a id="accuracy"></a>

## Accuracy

1:02:30 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

Should we be chasing after a level of accuracy that is extemely high even if we still have clinicians available?

> It is much harder to change practice. It is much easier to pop something in and say &ldquo;Hey I know you have to make these measurements, let me make them for you and you can look at them and see if you agree.&rdquo;

It is very hard to do something transformative so it&rsquo;s more of a how can we come together to come up with something better.


<a id="clinical-deployment"></a>

## Clinical Deployment

1:01:05 <sup><a id="fnr.7.100" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>

-   Code and weights from the video at bitbucket for surveillance predictions <sup><a id="fnr.15" class="footref" href="#fn.15" role="doc-backlink">15</a></sup>.
-   Routine measurements can be made in an automated way with a visual check of image segmentation quality.
-   Some automated diagnoses may happen at point-of-care, assessment of heart function, quick checks for rapid assessment.


<a id="future-improvements"></a>

## Future Improvements

-   Greater changes require much bigger datasets and more imaging than what is currently performed.
-   Pharmaceutical companies may have motivation for investing on performing higher quality imaging.
-   Surveillance of daily studies for identifying individuals eligible for clinical trials as a possibility in the future.
-   There are no powerful enough models to predict diseases at such a higher efficacy to change practice. So it is a collaborative process.

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://www.cdc.gov/dhdsp/pubs/guides/best-practices/clinical-decision-support.htm>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://www.analyticsvidhya.com/blog/2021/11/model-explainability/>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <https://en.wikipedia.org/wiki/Assay>

<sup><a id="fn.4" class="footnum" href="#fnr.4">4</a></sup> <https://en.wikipedia.org/wiki/Bland%E2%80%93Altman_plot>

<sup><a id="fn.5" class="footnum" href="#fnr.5">5</a></sup> <https://en.wikipedia.org/wiki/Gold_standard_(test)>

<sup><a id="fn.6" class="footnum" href="#fnr.6">6</a></sup> <https://cran.r-project.org/web/packages/blandr/vignettes/introduction.html>

<sup><a id="fn.7" class="footnum" href="#fnr.7">7</a></sup> <https://www.youtube.com/watch?v=MoEaRpLNo9A&ab_channel=MITOpenCourseWare>

<sup><a id="fn.8" class="footnum" href="#fnr.8">8</a></sup> <https://en.wikipedia.org/wiki/DICOM>

<sup><a id="fn.9" class="footnum" href="#fnr.9">9</a></sup> <https://pydicom.github.io/>

<sup><a id="fn.10" class="footnum" href="#fnr.10">10</a></sup> <https://towardsdatascience.com/understanding-dicom-bce665e62b72>

<sup><a id="fn.11" class="footnum" href="#fnr.11">11</a></sup> <https://www.osirix-viewer.com/>

<sup><a id="fn.12" class="footnum" href="#fnr.12">12</a></sup> <https://www.frontiersin.org/articles/10.3389/fcvm.2020.00025/full>

<sup><a id="fn.13" class="footnum" href="#fnr.13">13</a></sup> <https://www.sciencedirect.com/science/article/abs/pii/S1566253516301385>

<sup><a id="fn.14" class="footnum" href="#fnr.14">14</a></sup> <https://www.heart.org/en/health-topics/heart-failure/diagnosing-heart-failure/ejection-fraction-heart-failure-measurement>

<sup><a id="fn.15" class="footnum" href="#fnr.15">15</a></sup> <https://bitbucket.org/rahuldeo/echocv/src/master/>
