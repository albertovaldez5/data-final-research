#+title: Final Project Research
#+subtitle: Notes a bit over the place
#+author: Alberto Valdez
#+SETUPFILE: ../config/org-theme.config
#+SETUPFILE: ../config/org-header.config

If images don't show up, go [[https://gitlab.com/albertovaldez5/data-final-research/-/blob/main/docs/research.md][here.]]

* Kardias
:PROPERTIES:
:CUSTOM_ID: kardias
:END:

Kardias:
https://kardias.org/nosotros/

#+begin_quote
Somos una asociación civil dedicada a desarrollar programas de excelencia para la atención de niñas y niños con enfermedades cardíacas, así como a promover la educación y la difusión sobre la relevancia de las cardiopatías congénitas.
#+end_quote

Kardias Blog:
https://kardias.org/blog/

Friends of Kardias:
https://friendsofkardias.org/

** Kardias Report 2021
:PROPERTIES:
:CUSTOM_ID: kardias-report-2021
:END:

#+attr_html: :width 500px
[[../resources/kardias1.png]]

https://kardias.org/wp-content/uploads/2022/10/INFORME_2021_BAJA.pdf

El reporte de Kardias 2021 incluye datos sobre las diferentes consultas y estudios realizados.

#+attr_html: :width 500px
[[../resources/kardias2.png]]

#+attr_html: :width 500px
[[../resources/kardias3.png]]


* Tech Requirements
:PROPERTIES:
:CUSTOM_ID: tech-requirements
:END:

** Data Problems to Solve
:PROPERTIES:
:CUSTOM_ID: data-problems-to-solve
:END:

Current Data Storage:

1. CSV files?
2. DICOM files?
3. Database?
4. In-premises server?
5. IT / Web services?

** Deploy ML Models
:PROPERTIES:
:CUSTOM_ID: deploy-ml-models
:END:

Amazon Sagemaker.
https://aws.amazon.com/sagemaker/

Azure ML.
https://azure.microsoft.com/en-us/products/machine-learning/

** Alternative Hosting Services
:PROPERTIES:
:CUSTOM_ID: alternative-hosting-services
:END:

https://www.heroku.com/

Emergent alternatives for what was Heroku and cheaper than Azure, AWS, Google.

https://harperdb.io/

https://www.digitalocean.com/

https://www.mongodb.com/atlas

On-premises server? (highest security, long-term investment, will need IT consultant).

** AWS Healthcare & Life
:PROPERTIES:
:CUSTOM_ID: aws-healthcare-life
:END:

Secure and reliable deployment for data pipeline.

https://aws.amazon.com/health/machine-learning/?trk=65fe25a3-735f-43d5-8c08-6c5c926b009f&sc_channel=ps&s_kwcid=AL!4422!3!608595092146!p!!g!!machine%20learning%20and%20healthcare&ef_id=CjwKCAjwh4ObBhAzEiwAHzZYU_5fOG-nkcw7Bjoua9-VSvlZH_PwqlgKN306Et7LqCYQgxbIkCf_PhoCZkoQAvD_BwE:G:s&s_kwcid=AL!4422!3!608595092146!p!!g!!machine%20learning%20and%20healthcare

** Data Bootcamp Agenda
:PROPERTIES:
:CUSTOM_ID: data-bootcamp-agenda
:END:

Time and resources consideration.

- *First Segment:* Sketch It Out: Decide on your overall project, select your question, and build a simple model. You'll connect the model to a fabricated database, using comma-separated values (CSV) or JavaScript Object Notation (JSON) files, to prototype your idea.
- *Second Segment:* Build the Pieces: Train your model and build out the database you'll use for your final presentation.
- *Third Segment:* Plug It In: Connect your final database to your model, continue to train your model, and create your dashboard and presentation.
- *Fourth Segment:* Put It All Together: Put the final touches on your model, database, and dashboard. Lastly, create and deliver your final presentation to your class.

Rubric:

1. Presentation
2. GitHub repo
3. ML Model
4. Database Integration
5. Dashboard

** Data Bootcamp Tech Stack
:PROPERTIES:
:CUSTOM_ID: data-bootcamp-tech-stack
:END:

Example of a Tech Stack. Taken from the Data Analytics Bootcamp Materials.

#+begin_src md :eval no
# Technologies Used
## Data Cleaning and Analysis
Pandas will be used to clean the data and perform an exploratory analysis. Further analysis will be completed using Python.

## Database Storage
Mongo is the database we intend to use, and we will integrate Flask to display the data.

## Machine Learning
SciKitLearn is the ML library we'll be using to create a classifier. Our training and testing setup is ___. Extra ML verbiage here.

## Dashboard
In addition to using a Flask template, we will also integrate D3.js for a fully functioning and interactive dashboard. It will be hosted on ___.
#+end_src


* Learning about ML in Healthcare
:PROPERTIES:
:CUSTOM_ID: learning-about-ml-in-healthcare
:END:

** Clinical Decision Support Systems
:PROPERTIES:
:CUSTOM_ID: clinical-decision-support-systems
:END:

Clinical decision support systems (CDSS) are computer-based programs that analyze data within EHRs to provide prompts and reminders to assist health care providers in implementing evidence-based clinical guidelines at the point of care.  [fn:14]

Coursera 8 hours course (part of larger specialization).

https://www.coursera.org/learn/cdss4

** Explainability for A.I. in Healthcare
:PROPERTIES:
:CUSTOM_ID: explainability-for-a-i-in-healthcare
:END:

Model explainability refers to the concept of being able to understand the machine learning model. For example – If a healthcare model is predicting whether a patient is suffering from a particular disease or not. The medical practitioners need to know what parameters the model is taking into account or if the model contains any bias. So, it is necessary that once the model is deployed in the real world. Then, the model developers can explain the model. [fn:15]

Article on A.I. Explainability in Healthcare:

https://bmcmedinformdecismak.biomedcentral.com/articles/10.1186/s12911-020-01332-6

Explainability for ECG data.

https://modulai.io/blog/explainability-for-ecg-data/

ECG Classification.

https://www.kaggle.com/code/polomarco/ecg-classification-cnn-lstm-attention-mechanism

** Congenital Heart Disease
:PROPERTIES:
:CUSTOM_ID: congenital-heart-disease
:END:

Congenital heart disease is a general term for a range of birth defects that affect the normal way the heart works.

- https://www.nhs.uk/conditions/congenital-heart-disease/

- https://en.wikipedia.org/wiki/Congenital_heart_defect

** MIMIC-III Database
:PROPERTIES:
:CUSTOM_ID: mimic-iii-database
:END:

MIMIC-III is a large, freely-available database comprising deidentified health-related data associated with over forty thousand patients who stayed in critical care units of the Beth Israel Deaconess Medical Center between 2001 and 2012. The database includes information such as demographics, vital sign measurements made at the bedside (~1 data point per hour), laboratory test results, procedures, medications, caregiver notes, imaging reports, and mortality (including post-hospital discharge).

https://physionet.org/content/mimiciii/1.4/

** Privacy-Preserving Machine Learning (New Field)
:PROPERTIES:
:CUSTOM_ID: privacy-preserving-machine-learning-new-field
:END:

*PPML (Research):*

The PPML initiative was started in partnership between Microsoft Research and Microsoft product teams with the objective of protecting the confidentiality and privacy of customer data when training large-capacity language models.

https://www.microsoft.com/en-us/research/blog/privacy-preserving-machine-learning-maintaining-confidentiality-and-preserving-trust/
*Predictive Analysis in Encrypted Data (Paper):*

Privacy of sensitive information can be guaranteed, if it is encrypted by the data owner before
being uploaded to a cloud service. In that way, only the legitimate data owner can access the
data by decrypting it using their private decryption key.

https://www.microsoft.com/en-us/research/wp-content/uploads/2014/08/336-1.pdf


* Biometrics and Analytics
:PROPERTIES:
:CUSTOM_ID: biometrics-and-analytics
:END:

** Bland-Altman plot
:PROPERTIES:
:CUSTOM_ID: bland-altman-plot
:END:

Bland-Altman plot is a method of data plotting used in analyzing the agreement between two =assays= [fn:9] [fn:8].

Bland–Altman plots are extensively used to evaluate the agreement among two different instruments or two measurements techniques. It can be used to compare a new measurement technique or method with a =gold standard= [fn:10]. Bland-Altman plots are available in R libraries [fn:11].

#+attr_html: :width 500px
[[https://d2mvzyuse3lwjc.cloudfront.net/doc/en/UserGuide/images/Bland_Altman_Plots/UG_appendix_bland_altman_plot.png]]


* MIT Class: Applications of ML in Cardiac Imaging
:PROPERTIES:
:CUSTOM_ID: mit-class-applications-of-ml-in-cardiac-imaging
:END:

https://www.youtube.com/watch?v=MoEaRpLNo9A&ab_channel=MITOpenCourseWare

MIT 6.S897 Machine Learning for Healthcare, Spring 2019
Instructor: Rahul Deo.

Guest lecturer Rahul Deo, the lead investigator of the One Brave Idea project at Brigham and Women's Hospital, talks about how machine learning techniques are being used and can be used further to augment cardiac imaging.

These notes follow the video:

** Types of Heart Diseases
:PROPERTIES:
:CUSTOM_ID: types-of-heart-diseases
:END:

10:20 [fn:1]

| Abnormality           | Disease                                                                    | Presentation                                    | Treatment                                            |
|-----------------------+----------------------------------------------------------------------------+-------------------------------------------------+------------------------------------------------------|
| Contractile function  | Heart failure                                                              | Shortness of breath, fluid buildup in legs      | Medications, ventricular assist device, transplant   |
| Coronary blood supply | Coronary artery disease, myocardial infraction                             | Chest pain, shortness of breath                 | Angioplasty/stening, coronary artery bypass grafting |
| Circulatory Flow      | Aortic stenosis/regurgitation, mitral stenosis/regurgitation.              | Light headedness, shortness of breath, fainting | valve replacement, valve repair                      |
| Heart rhythm          | Arterial fibrillation/flutter, ventricular tachycardia, sick sinus synrome | palpitations, fainting, cardiac arrest          | ablation, implantable defibrillator, pacemaker       |

** Cardiac imaging technologies
:PROPERTIES:
:CUSTOM_ID: cardiac-imaging-technologies
:END:

12:42 [fn:1]

| Modality                         | Cost      | Approach                 | Diagnostic Utility                                            |
|----------------------------------+-----------+--------------------------+---------------------------------------------------------------|
| Electrocardiogram (ECG)          | low       | Voltage Differences      | Mycocardial infarction                                        |
| Echocardiography                 | mid-high  | Ultrasound (sound waves) | Quantitation of cardiac structure and function, heart failure, valvular disease, pulmonary hypertension |
| MRI                              | very high | Magnetic resonance       | Quantitation of cardiac structure and function, heart failure, valvular disease |
| Angiography                      | very high | X-ray                    | Epicardial coronary artery disease                            |
| SPECT/PET (Non-invasive)         | very high | Radionuclide tracer      | Coronary artery disease, microvascular disease                |
| Intracardiac pressure transducer | high      | Pressure transucer       | Heart failure, valvular disease, pulmonary hypertension       |

** Cardiac decisions guided by inputs from imaging
:PROPERTIES:
:CUSTOM_ID: cardiac-decisions-guided-by-inputs-from-imaging
:END:

14:25 [fn:1]

| Disease                 | Decision                                                      | Inputs                                                        |
|-------------------------+---------------------------------------------------------------+---------------------------------------------------------------|
| heart failure           | decision to implant a defibrilator to prevent sudden death    | Symptoms + ejection fraction of the heart <35%                |
| Coronary artery disease | Angioplasty and stenting of a coronary artery                 | Symptoms + stenosis > 70%                                     |
| Aortic stenosis         | Valve replacement                                             | Symptoms + valve are + enlargement of the heart               |
| Atrial fibrillation     | Decision to start anticoagulation to prevent stroke           | Age, sex, other diagnoses                                     |
| Mycrocardial infraction | Decision to start aspirin and a statin to prevent a future heart attack | A risk model based on age, sex, lab values, blood pressure, diabetes |

** How Medical Imaging Data Are Stored
:PROPERTIES:
:CUSTOM_ID: how-medical-imaging-data-are-stored
:END:

*DICOM* [fn:2] is the international standard to transmit, store, retrieve, print, process and display imaging information.

#+attr_html: :width 500px
[[https://www.radiantviewer.com/dicom-viewer-manual/images/radiant_dicom_viewer_dicom_tags_window.png]]

Image/video files are stored in DICOM format and combine a compressed image with a DICOM header which includes characteristics of the image.

Open access libraries like GDCM, pydicom facilitate compressing/uncompressing, reading and editing headers. [fn:3] More info on using PyDicom. [fn:5]

Osirix is a high-end medical images viewer [fn:4].

** Access to Data
:PROPERTIES:
:CUSTOM_ID: access-to-data
:END:

Most imaging data is housed in data archives, not very accessible to do Machine Learning / Data processes. Access is often highly limited, some images contain patient information. Labels and measurements are often stored separately in the electronic health record.

*Challenge*: It is expensive to collect this type of data and the data available may be limited. The higher the costs of the study/imaging, the less data available.

** Characteristics of Cardiac Imaging Data
:PROPERTIES:
:CUSTOM_ID: characteristics-of-cardiac-imaging-data
:END:

18:00 [fn:1]

1. Compression: lossy vs losseless.
2. Spatial resolution: pixel dimension.
3. Sampling Frequency: temporal resolution.
4. Coronary artery velocity is 10-65 mm/seconds.
5. Gating: Electrocardiogram info. can be coupled with imaging information to average images accross corresponding portions of the cycle (a sort of workaround with missing data, low frequency resolution).

** Interpreting data
:PROPERTIES:
:CUSTOM_ID: interpreting-data
:END:

19:00 [fn:1]

The current way to interpret the data is to measure it "by hand". So diagnosis is made by looking at the images. Machine Learning can help with Image/Video classification, Semantic Segmentation, Image registration (all use Convolutional Neural Networks).

If a radiologist takes 2 minutes to read a study, how much benefit is the to automate the process?

Some answers: In some scenarios like overnight/weekend diagnosis/reading it can be helpful. An independent read can catch some missed diagnoses.

Example of DL Imagen Segmentation. [fn:6]

#+attr_html: :width 500px
[[https://www.frontiersin.org/files/Articles/508599/fcvm-07-00025-HTML/image_m/fcvm-07-00025-g001.jpg]]

** Solutions to look for
:PROPERTIES:
:CUSTOM_ID: solutions-to-look-for
:END:

36:05 [fn:1]

1. Low-cost quantitative metrics: track indicators of disease progression and reflect the onset of tissue-level changes.
2. Specific to the disease process.
3. Combine resultant data with therapy that matches the report, not only numbers (not in our hands as analytics).

Example quantitative measures:

1. Simple cardiac ultrasound views provide a quantitative metric of early disease progression (changes on ventricular mass, volume, etc.)

** Low risk - high reward automated interpretation
:PROPERTIES:
:CUSTOM_ID: low-risk-high-reward-automated-interpretation
:END:

38:00 [fn:1]

What kind of medical procedures or applications can automated-metrics accompany? Low liability and that can help the patients in not-so-critical/high-skilled tasks.

| Low liability                                               | High liability                      |
|-------------------------------------------------------------+-------------------------------------|
| Non-skilled acquisition (primary care)                      | Skilled sonographer                 |
| Low cost handheld ultrasound                                | Costly full ultrasound system       |
| Automated Interpretation                                    | Expert Cardiologist interpretation  |
| Early in disease course                                     | Late in disease course              |
| Decision support (initiation or intensification of therapy) | Difficult decision regarding sugery |

** What should we focus on?
:PROPERTIES:
:CUSTOM_ID: what-should-we-focus-on
:END:

39:00 [fn:1]

- Automating interpretation to reduce costs.
- Low-cost quantitative metrics that are indicative of disease progression and reflect the onset of these tissue-level changes.
- Recognizing meaningful subclasses of diseases. Capture complex underlying biological processes.

*Rapid triage*: Detecting markers of myocardial infarction pattern in electrocardiogram (ECG). Offload rapid triage decisions to an automated system that can be used by non-cardiologists.

*Echocardiography* classification [fn:7].

** How do we show improvement?
:PROPERTIES:
:CUSTOM_ID: how-do-we-show-improvement
:END:

50:33 [fn:1]

There is a wide variability in measurement values from physician to physician, up to 8-9% for the ejection fraction [fn:12], the ejection fraction normal value is around 60%.

1. Compare to an average of multiple readers.
2. Compare to a gold-standard imaging system (MRI).
3. Demonstrate utility in outcomes (trials).

** Automated Disease Detection Models
:PROPERTIES:
:CUSTOM_ID: automated-disease-detection-models
:END:

54:03 [fn:1]

Several rare diseases would benefit from referral to a cardiologist or specialty center. These diagnoses tend to be missed at centers that see them infrequently. ML models can serve as "decision support".

#+attr_html: :width 500px
[[../resources/mit2.png]]

#+attr_html: :width 500px
[[../resources/mit3.png]]

#+attr_html: :width 500px
[[../resources/mit4.png]]

** Sensitivity
:PROPERTIES:
:CUSTOM_ID: sensitivity
:END:

57:55 [fn:1]

What should be the True-Positive False-Positive Trade-off?

#+begin_quote
Air on the side of higher sensitivity.
#+end_quote

Some of the models are for pretty rare diseases, so it depends how much you want cardiologists to go back and take a look at some studies/cases again. Depends on the costs of going back and looking at cases that may be false positives. But if the sensitivity is good, we can make the case for spending the time and money to revisit them.

** Accuracy
:PROPERTIES:
:CUSTOM_ID: accuracy
:END:

1:02:30 [fn:1]

Should we be chasing after a level of accuracy that is extemely high even if we still have clinicians available?

#+begin_quote
It is much harder to change practice. It is much easier to pop something in and say "Hey I know you have to make these measurements, let me make them for you and you can look at them and see if you agree."
#+end_quote

It is very hard to do something transformative so it's more of a how can we come together to come up with something better.

** Clinical Deployment
:PROPERTIES:
:CUSTOM_ID: clinical-deployment
:END:

1:01:05 [fn:1]

- Code and weights from the video at bitbucket for surveillance predictions [fn:13].
- Routine measurements can be made in an automated way with a visual check of image segmentation quality.
- Some automated diagnoses may happen at point-of-care, assessment of heart function, quick checks for rapid assessment.

** Future Improvements
:PROPERTIES:
:CUSTOM_ID: future-improvements
:END:

- Greater changes require much bigger datasets and more imaging than what is currently performed.
- Pharmaceutical companies may have motivation for investing on performing higher quality imaging.
- Surveillance of daily studies for identifying individuals eligible for clinical trials as a possibility in the future.
- There are no powerful enough models to predict diseases at such a higher efficacy to change practice. So it is a collaborative process.


* Footnotes
:PROPERTIES:
:CUSTOM_ID: footnotes
:END:

[fn:15]https://www.analyticsvidhya.com/blog/2021/11/model-explainability/
[fn:14]https://www.cdc.gov/dhdsp/pubs/guides/best-practices/clinical-decision-support.htm

[fn:13]https://bitbucket.org/rahuldeo/echocv/src/master/
[fn:12]https://www.heart.org/en/health-topics/heart-failure/diagnosing-heart-failure/ejection-fraction-heart-failure-measurement

[fn:11]https://cran.r-project.org/web/packages/blandr/vignettes/introduction.html
[fn:10]https://en.wikipedia.org/wiki/Gold_standard_(test)
[fn:9]https://en.wikipedia.org/wiki/Assay
[fn:8]https://en.wikipedia.org/wiki/Bland%E2%80%93Altman_plot

[fn:7]https://www.sciencedirect.com/science/article/abs/pii/S1566253516301385
[fn:6]https://www.frontiersin.org/articles/10.3389/fcvm.2020.00025/full

[fn:5]https://towardsdatascience.com/understanding-dicom-bce665e62b72
[fn:4]https://www.osirix-viewer.com/

[fn:3]https://pydicom.github.io/

[fn:2]https://en.wikipedia.org/wiki/DICOM

[fn:1]https://www.youtube.com/watch?v=MoEaRpLNo9A&ab_channel=MITOpenCourseWare
